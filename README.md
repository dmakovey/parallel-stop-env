# parallel-stop-env

experiment with parallel pipelines and environment stop etc.

## Tests results

### 1

385030ec1cf94d34205f5142c31c50ae1f314fc2

```
create_job: [1] job: on_stop job stop_job is not defined
```

### 2

3549bb378118d49ded24e27562554fb09f96edb0

```yaml
environment: &environment_block
    name: my_env-${INSTANCE}
    on_stop: "stop_job [${INSTANCE}]"
```

```
create_job: [1] job: on_stop job stop_job [${INSTANCE}] is not defined
```

